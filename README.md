
SecurityPoc是一款基于Pocsuite3开发的POC插件扫描器，遵循高内聚、低耦合、轻量级poc管理工具，集成等保基线检测，应急响应
使用django做框架 web采用layui 等界面友好的集成了支持扩展模块、扩展POC等功能，智能雷达能自动加载poc用fafo自动加载目标进行探测！

* 功能:

      
1，	pocsuit3
2，	安全基线
3，	应急响应
4，	目标扫描
5， 终端操作1. webssh,  远程桌面 (依赖guacamole服务端)。2，陆续审计
6，	运行报告
7，	威胁情报
* 环境：  基于django、python3.8开发。 
        Centos7.6
        python3.8
        django2.2
        mysql5.7

* 部署：
        pip3 install -r requirements.txt, 

* app模块：


# 展望：
对接自动化测试，后期集成到soc安全运营平台，并融合态势感知等。

# 感谢：
本系统参考了一大堆安全工具，在此不一一列举。
<a href="https://github.com/jumpserver/jumpserver" target="_blank">jumpserver</a>  
<a href="https://github.com/jimmy201602/webterminal" target="_blank">webterminal</a>  
<a href="https://www.layui.com/" target="_blank">layui</a>  
<a href="https://echarts.apache.org/zh/index.html" target="_blank">webterminal</a> 


